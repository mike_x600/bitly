# notes

~~~~
Install-Package Microsoft.AspNet.WebApi.OwinSelfHost

Install-Package mongocsharpdriver -Version 2.3 
~~~~


~~~~
show dbs
$ 	admin    0.000GB
	foo      0.000GB
	local    0.000GB
	records  0.000GB
	
use foo
$ switched to db foo

db.getCollectionInfos();
$ [
	{
		"name" : "exampledata",
		"type" : "collection",
		"options" : {},
		"info" : {	"readOnly" : false },
		"idIndex" : { ... }
	},
	{
		"name" : "something",
		"type" : "collection",
		"options" : {},
		"info" : {	"readOnly" : false },
		"idIndex" : { ... }
	}
]

db.getCollectionInfos()[1]["name"];
$ something

db.something.insert({"example":"data"});
$ WriteResult({ "nInserted" : 1 })

db.dropDatabase();
$ { "dropped" : "foo", "ok" : 1 }

db.getCollectionNames()
$ [ "collection_url" ]

~~~~