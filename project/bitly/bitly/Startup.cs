﻿/*
 * Created by SharpDevelop.
 * User: mm
 * Date: 10/29/2019
 * Time: 4:37 AM
 */
using System;
using Owin; 
using System.Web.Http;


namespace bitly
{
	public class Startup 
    {
		
        public void Configuration(IAppBuilder appBuilder) 
        { 
            HttpConfiguration config = new HttpConfiguration();
            
            config.Routes.MapHttpRoute( 
                name: "indexRoute", 
                routeTemplate: "", 
                defaults: new { 
                	controller="default",
                	action="showIndex"
                }
            );

			config.Routes.MapHttpRoute( 
                name: "getTinyLink", 
                routeTemplate: "get",
                defaults: new { 
                	controller="default",
                	action="getTinyLink"
                }
            );

			config.Routes.MapHttpRoute( 
                name: "getFullLink", 
                routeTemplate: "link/{link}", 
                defaults: new { 
                	controller="default",
                	action="getFullLink",
                	link = RouteParameter.Optional
                }
            );             

			config.Routes.MapHttpRoute( 
                name: "GetList", 
                routeTemplate: "getlist", 
                defaults: new { 
                	controller="default",
                	action="showGetList"
                }
            );
            
            config.Routes.MapHttpRoute( 
                name: "showStatic", 
                routeTemplate: "static/{file}", 
                defaults: new { 
                	controller="default",
                	action="showStatic",
                	file = RouteParameter.Optional
                }
            );

            appBuilder.UseWebApi(config);
        } 
    } 
}
