﻿/*
 * Created by SharpDevelop.
 * User: mm
 * Date: 10/29/2019
 * Time: 4:30 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using bitly.Tests;
using Microsoft.Owin.Hosting;
using System.Net.Http;

using System.Diagnostics;
using System.Text.RegularExpressions;

using System.Threading;

namespace bitly
{
	class Program
	{
		static public string baseAddress = "http://localhost:9000/";
		
		static public string mongodb_connection_host = "10.0.2.2";
		static public int mongodb_connection_port = 27017;
		
		static public bool baseUseConsoleLog = true;
		
		public static void Main(string[] args)
		{			
			Console.WriteLine("started application");
			
			foreach(var arg in args){
        		Console.WriteLine("::{0}", arg );
        		if (arg.Split('=').Length > 1){
        			var arg_name = arg.Split('=')[0];
        			var arg_value = arg.Split('=')[1];        			
					Console.WriteLine(":: ::{0} {1}", arg_name, arg_value );
        			
        			switch( arg_name.Trim() ){
        				case "host":
        					baseAddress = arg_value.Trim();
        					break;
    					case "mongo_host":
        					mongodb_connection_host = arg_value.Trim();
        					break;
    					case "mongo_port":
        					mongodb_connection_port = Convert.ToInt32( arg_value.Trim() );
        					break;
        			}
        		}
			}
			
			Console.WriteLine("configuration done");
			
			Console.WriteLine("baseAddress: " + baseAddress );
			Console.WriteLine("mongodb host: " + mongodb_connection_host );
			Console.WriteLine("mongodb port: " + mongodb_connection_port);
			
			using (WebApp.Start<Startup>(url: baseAddress)) 
            {                
				//TestCommon.test_try_make_request(baseAddress);
                //TestCommon.test_url_check_recognition();
                //TestCommon.test_mongodb();
                
                while(true){
                	Thread.Sleep(5);
                }
                //Console.ReadLine();
            } 
			
			Console.WriteLine("execution done");			
		}		
	}
}