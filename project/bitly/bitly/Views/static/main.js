﻿$(document).ready(function() {
	console.log("started");
	$("#btn_try_again").click(function() {
		$("#url_form").fadeIn();
		$("#url_form_result").fadeOut();
	});
	
	$("#btn_short_make").click(function() {
		console.log("make some ajax");
		$.post( "get/", { link: $("#url_form_content").val() }, function(data){
			console.log(data);
			if(data.success){
				$("#url_form_result").fadeIn();
				
				$("#url_form_result").find("h2").text( "Success!" );
				$("#url_form_result").find("div").text( data.message );
				
				$("#url_form").fadeOut();
			}else{
				$("#url_form_result").fadeIn();
				
				$("#url_form_result").find("h2").text( "Fail!" );
				$("#url_form_result").find("div").text( data.message );
				
				$("#url_form").fadeOut();
			}
		});
	});
});