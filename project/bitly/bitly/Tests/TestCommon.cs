﻿/*
 * Created by SharpDevelop.
 * User: mm
 * Date: 10/30/2019
 * Time: 6:31 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Net.Http;

using MongoDB.Bson;
using MongoDB.Driver;

namespace bitly.Tests
{
	/// <summary>
	/// Description of TestCommon.
	/// </summary>
	public static class TestCommon
	{		
		public static string[] url_arr = {"https://www.example.com",
				"http://www.example.com",
				"www.example.com",
				"example.com",
				"http://blog.example.com",
				"http://www.example.com/product",
				"http://www.example.com/products?id=1&page=2",
				"http://www.example.com#up",
				"http://255.255.255.255",
				"255.255.255.255",
				"http://invalid.com/perl.cgi?key= | http://web-site.com/cgi-bin/perl.cgi?key1=value1&key2",
				"http://www.site.com:8008",
				"someotherstring",
				"255.255.wtf.255",
				"",
				"??"				
		};
		
		public static void test_url_check_recognition(){
			foreach( string _url in TestCommon.url_arr){
				Console.WriteLine( Utils.check_url( _url ) + " " + _url );
			}
		}
		
		public static void test_mongodb(){
			//localhost
			//10.0.2.2
			//10.0.2.3
			
			//var client = new MongoClient("mongodb://10.0.2.2:27017");
			
			
			try{
				var client = new MongoClient(new MongoClientSettings
				{
			       Server = new MongoServerAddress(Program.mongodb_connection_host, Program.mongodb_connection_port),
			       ClusterConfigurator = builder =>
			       {
			             builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(1)));
			       }				
				});
				
				var database = client.GetDatabase("records");
				var collection = database.GetCollection<BsonDocument>("collection_url");
										
				BsonDocument d = new BsonDocument();
				
				d.Add("session_id", "commontest");
				d.Add("url_full", "http://yandex.ru");
				d.Add("url_tiny_part", "001h1hbb78");
				d.Add("url_created", DateTime.UtcNow.ToString("dd.MM.yyyy HH:mm:sss") );
				
				collection.InsertOneAsync(d).Wait();
				
	
				var list = collection.Find(new BsonDocument()).ToListAsync().Result;
				
				foreach(var document in list)
				{
				    Console.WriteLine(document);
				}
			
			}catch(System.AggregateException ex){
				Console.WriteLine("Failed to exec mongodb");
				return;
			}
		}
		
		public static void test_try_make_request(string baseAddress){
			HttpClient client = new HttpClient(); 

            var response = client.GetAsync(baseAddress).Result; 
			//Console.WriteLine(response); 
            //Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            //Console.WriteLine("");                
            
            response = client.GetAsync(baseAddress + "/get/somestringhere").Result; 
			//Console.WriteLine(response); 
            //Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            //Console.WriteLine("");
            
            response = client.GetAsync(baseAddress + "/link/123").Result; 
			//Console.WriteLine(response); 
            //Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            //Console.WriteLine("");

			response = client.GetAsync(baseAddress + "/getlist").Result; 
			//Console.WriteLine(response); 
            //Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            //Console.WriteLine("");
		}
	}
}
