﻿/*
 * Created by SharpDevelop.
 * User: mm
 * Date: 10/29/2019
 * Time: 4:46 AM
 */
using System;
using System.Collections.Generic;
using System.Text;

using System.Reflection;

using System.Web;
using System.Web.Http;
using System.Web.Razor.Parser;
using System.Web.Services;


using System.Collections.Generic;
using System.Data;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

using System.Diagnostics;

using System.Globalization;
using Newtonsoft.Json;

using RazorEngine;
using RazorEngine.Templating;
using RazorEngine.Configuration;

namespace bitly
{
	public class DataTinyLinkRespFormat{
		public string url_tiny;
		public string url_fill;
		public bool success;
		public string message;
	}
	public class DefaultController : ApiController 
    { 
		
		private IRazorEngineService razorEngineService;
		
		DefaultController(){			
			Console.WriteLine("started controller");
			
			TemplateServiceConfiguration templateConfig = new TemplateServiceConfiguration();
            templateConfig.Debug = false;
            
			string templateDefaultIndex = GetEmbeddedResourceContent("bitly.Views.DefaultIndex.cshtml");
            
			razorEngineService = RazorEngineService.Create((ITemplateServiceConfiguration)templateConfig);
            razorEngineService.AddTemplate("DefaultIndex.cshtml", templateDefaultIndex);
            razorEngineService.Compile("DefaultIndex.cshtml");            
		}
		
		[HttpGet, ActionName("showIndex")]
		public HttpResponseMessage showIndex(HttpRequestMessage request)
		{
			Utils.debug_action_log( "Index", "text/html" );
			string session_id = Utils.session_get(request);
			// processing content
			
			
			string content = "";			
			//content = Views.get_index_html();
			
			content = razorEngineService.RunCompile("DefaultIndex.cshtml", 
                model: new { 
                        	Name = "World",
                        	Title = "bitly service", 
                        	baseAddress = Program.baseAddress, 
                        	showForm = true , 
                        	showList = false ,
                        	showListContent = new {}
                        });
			
    		// output content
		    var response = new HttpResponseMessage();
			response.Content = new StringContent(content);
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");			
			Utils.session_setup( response, request.RequestUri.Host, session_id );			
		    return response;
		}
    			
		[HttpGet]
		[HttpPost]
    	public HttpResponseMessage getTinyLink(HttpRequestMessage request)
		{
    		Utils.debug_action_log( "getTinyLink", "application/json" );
    		string session_id = Utils.session_get(request);
			// processing content
    		
    		string link = "";
    		
    		if(request.Method == HttpMethod.Post){
    			Console.WriteLine("POST:");
    			
    			var provider = request.Content.ReadAsFormDataAsync().Result;
				foreach(string _it in provider){
    				if( _it == "link"){
    						link = provider.Get(_it);
    					}
    			}    			
    		}else if(request.Method == HttpMethod.Get){
    			Console.WriteLine("GET:");
    			IEnumerable<KeyValuePair<string, string>> queryString = request.GetQueryNameValuePairs();
    			
    			foreach(var _it in queryString){
    				if(  _it.Key == "link"){
    					link = _it.Value;
    				}
    			}
    		}
        	
        	Console.WriteLine( link );
        	
        	JsonSerializer ser = new JsonSerializer();		 	
		 	var resp = new DataTinyLinkRespFormat();
		 	resp.success = false;
		 	resp.message = "";
		 	resp.url_fill = link;
        	
		 	if( Utils.check_url(link) ){
		 		string url_tine_part = Utils.url_get_tiny_part();
		 		resp.url_tiny = Program.baseAddress + url_tine_part;
		 		
		 		if( Utils.url_make_tiny(link, url_tine_part, session_id) ){
		 			resp.success = true;
		 			resp.message = "Nice url!";		 			
		 		}else{
		 			resp.success = false;
		 			resp.message = "Fail to add such url";
		 		}
		 	}else{
		 		resp.success = false;
	 			resp.message = "Not valid URL format. Sorry.";
	 		}
		 	
		    
			// output content
		    var response = new HttpResponseMessage { 
				Content = new StringContent(JsonConvert.SerializeObject(resp) , 
                System.Text.Encoding.UTF8, 
                "application/json") };			
			Utils.session_setup( response, request.RequestUri.Host, session_id );			
		    return response;
		}    	
    	
    	[HttpGet]
    	public HttpResponseMessage getFullLink(HttpRequestMessage request)
		{
    		Utils.debug_action_log( "getFullLink", "application/json" );
    		string session_id = Utils.session_get(request);
			// processing content
    		
			string uri = request.RequestUri.ToString();
			string tiny = uri.Substring( uri.LastIndexOf("/") + 1 ).Trim();
			string full_url = Utils.url_get_full(tiny);
			
			string content = "getFullLink:" + tiny;
			content += "\r\n";
			content += Utils.url_get_full(tiny);
    		
    		// output content
    		var response = new HttpResponseMessage();
    		if( Utils.check_url(full_url) ){
    			response.StatusCode = HttpStatusCode.Moved;
				response.Headers.Location = new Uri(full_url);  
			    Utils.session_setup( response, request.RequestUri.Host, session_id );
			}else{
				response.Content = new StringContent(content);
			    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
			    Utils.session_setup( response, request.RequestUri.Host, session_id );
			}
		    
		    return response;
		}
    	
    	[HttpGet]
    	public HttpResponseMessage showGetList(HttpRequestMessage request)
		{
    		Utils.debug_action_log("showGetList", "text/html" );
    		string session_id = Utils.session_get(request);
			// processing content
			
    		
    		string content = "";
    		
    		List<KeyValuePair<string,string>> output = Utils.url_list(session_id);
    		
			content = razorEngineService.RunCompile("DefaultIndex.cshtml", 
                model: new { 
                	Name = "",
                	Title = "bitly service",
                	baseAddress = Program.baseAddress,
                	showForm = false,
                	showList = true,
                	showListContent = output
            });
    		
    		// output content
    		var response = new HttpResponseMessage();
		    response.Content = new StringContent(content);
		    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
		    Utils.session_setup( response, request.RequestUri.Host, session_id );	
		    return response;
		}
    	
    	[HttpGet]
    	public HttpResponseMessage showStatic(string file){
    		Console.WriteLine(file);
    		    		
    		string file_content = "";
    		string file_mime = "";
    		
    		switch(file){
    			case "main.js":
    				file_content = GetEmbeddedResourceContent("bitly.Views.static.main.js");
    				file_mime = "text/javascript";
    				break;
				case "jquery.js":
    				file_content = GetEmbeddedResourceContent("bitly.Views.static.jquery.js");
    				file_mime = "text/javascript";
    				break;
				case "main.css":
    				file_content = GetEmbeddedResourceContent("bitly.Views.static.main.css");
    				file_mime = "text/css";
    				break;
    			default:
    				file_content = "any other resource files?";
    				file_mime = "text/html";
    				break;    		
    		}
    			
    		// output content
    		var response = new HttpResponseMessage();
		    response.Content = new StringContent(file_content);
		    response.Content.Headers.ContentType = new MediaTypeHeaderValue(file_mime);
		    return response;
    	}    	
    	
    	public static string GetEmbeddedResourceContent(string resourceName)
		{    		
		    Assembly asm = Assembly.GetExecutingAssembly();
		    Stream stream = asm.GetManifestResourceStream(resourceName);
		    StreamReader source = new StreamReader(stream);
		    string fileContent = source.ReadToEnd();
		    source.Dispose();
		    stream.Dispose();
		    return fileContent;
		}
    } 
}
