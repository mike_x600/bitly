﻿/*
 * Created by SharpDevelop.
 * User: mm
 * Date: 10/30/2019
 * Time: 7:54 AM
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using System.Linq;

using System.Web;
using System.Web.Http;
using System.Web.Services;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using MongoDB.Bson;
using MongoDB.Driver;

namespace bitly
{
	public static class Utils
	{
		
		public static void debug_action_log(string str_action, string str_type ) {
			if(!Program.baseUseConsoleLog){return;}
			Console.Write( String.Format("#{0} #action:{1}\n", DateTime.Now, str_action ) );    		
    		Console.Write( String.Format("\tcontent-type:{0}\n", str_type ) );
		}
		
		public static bool check_url(string _url){
			Uri uriResult;
			
			_url = _url.Trim();
			
			if(_url.Split(' ').Length > 1){
				return false;
			}
			
			bool passed = Uri.TryCreate( _url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
			return passed;
		}
		
		public static string session_generate(){
    		StringBuilder builder = new StringBuilder();  
		    Random random = new Random();  
		    char ch;  
		    
		    for (int i = 0; i < 16; i++)  
		    {  
		        ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));  
		        builder.Append(ch);  
		    }  
	        return builder.ToString().ToLower();
    	}
    	
    	public static void session_setup(HttpResponseMessage response, string host, string session_id){
    		CookieHeaderValue cookie = new CookieHeaderValue("session", session_id );
			cookie.Expires = DateTimeOffset.Now.AddDays(1);
			cookie.Domain = host; //Request.RequestUri.Host;
			cookie.Path = "/";
			response.Headers.AddCookies(new CookieHeaderValue[] { cookie }); 
    	}
    	
    	public static string session_get(HttpRequestMessage request){
    		string session_id = "";

			CookieHeaderValue cookie = request.Headers.GetCookies("session").FirstOrDefault();
			
			if (cookie != null){
			    session_id = cookie["session"].Value;
			}
			
			if(session_id.Length > 0){
				Console.WriteLine("\tsession: " + session_id);
			}else{
				session_id = session_generate();
				Console.WriteLine("\tsession: " + session_id + "(generated)");
			}			
			
			return session_id;
    	}
		
		public static string url_get_tiny_part(){
			StringBuilder string_builder = new StringBuilder();  
	    	Random random = new Random();
			
			var client = new MongoClient(new MongoClientSettings
			{
		       Server = new MongoServerAddress(Program.mongodb_connection_host, Program.mongodb_connection_port),
		       ClusterConfigurator = builder =>
		       {
		             builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(1)));
		       }				
			});
			
			var database = client.GetDatabase("records");
			var collection = database.GetCollection<BsonDocument>("collection_url");
			
			while(true){
		    	char ch;
		    	
		    	string_builder.Clear();
		    
			    for (int i = 0; i < 12; i++)  
			    {  
			        ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));  
			        string_builder.Append(ch);  
			    }  
			    
			    var list = collection.Find(new BsonDocument("url_tiny_part", string_builder.ToString() )).ToListAsync().Result;
			    
				if(list.Count == 0){break;}
			}
			
			return string_builder.ToString();
		}
		
		public static bool url_make_tiny(string url_source, string url_tiny, string session_id){
			var client = new MongoClient(new MongoClientSettings
			{
		       Server = new MongoServerAddress(Program.mongodb_connection_host, Program.mongodb_connection_port),
		       ClusterConfigurator = builder =>
		       {
		             builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(1)));
		       }				
			});
			
			var database = client.GetDatabase("records");
			var collection = database.GetCollection<BsonDocument>("collection_url");
						
			BsonDocument d = new BsonDocument();
			
			d.Add("session_id", session_id );
			d.Add("url_full", url_source);
			d.Add("url_tiny_part", url_tiny );
			d.Add("url_created", DateTime.UtcNow.ToString("dd.MM.yyyy HH:mm:sss") );
			
			collection.InsertOneAsync(d).Wait();
			
			return true;
		}
		
		public static string url_get_full(string url_tiny){
			var client = new MongoClient(new MongoClientSettings
			{
		       Server = new MongoServerAddress(Program.mongodb_connection_host, Program.mongodb_connection_port),
		       ClusterConfigurator = builder =>
		       {
		             builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(1)));
		       }				
			});
			
			var database = client.GetDatabase("records");
			var collection = database.GetCollection<BsonDocument>("collection_url");
			
			var list = collection.Find(new BsonDocument("url_tiny_part", url_tiny )).ToListAsync().Result;
			
			string url_full = "";				
			foreach(var document in list)
			{
				BsonValue string_url_full;
				if(document.TryGetValue("url_full",out string_url_full)){
					url_full = string_url_full.ToString();
				}
			}
			return url_full;
		}
		
		public static List<KeyValuePair<string,string>> url_list(string session_id){
			var client = new MongoClient(new MongoClientSettings
			{
		       Server = new MongoServerAddress(Program.mongodb_connection_host, Program.mongodb_connection_port),
		       ClusterConfigurator = builder =>
		       {
		             builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(1)));
		       }				
			});
			
			var database = client.GetDatabase("records");
			var collection = database.GetCollection<BsonDocument>("collection_url");
			
			var search = new BsonDocument("session_id", session_id );
			Console.WriteLine("search by session_id: " + session_id);
			
			var list = collection.Find(search).ToListAsync().Result;
			var kv_buffer = new List<KeyValuePair<string,string>>();
			
			foreach(var document in list)
			{
				BsonValue string_url_tiny_part;
				BsonValue string_url_full;
				
				if( document.TryGetValue("url_tiny_part",out string_url_tiny_part) &&
				  	document.TryGetValue("url_full",out string_url_full) ){
					
					kv_buffer.Add( new KeyValuePair<string, string>(
						string_url_tiny_part.ToString(), 
						string_url_full.ToString() ) );
				}
			    Console.WriteLine(document);
			}
			
			return kv_buffer;
		}
	}
}
